f2 = lambda: print("hello")
f2()

def even(x):
    return x % 2 == 0

l1 = [1,2,3,4,5,6,7]
print(list(filter(even, l1))) # even must return bool
print(list(filter(lambda x: x % 2 == 0, l1))) # even must return bool

def biggerthan50(x):
    return x > 50
l2 = range(100)
print(list(filter(biggerthan50, l2)))
print(list(filter(lambda x: x > 50, l2)))
#print list filter only items which are bigger than 50