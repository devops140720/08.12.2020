import requests
import json

url = "http://localhost:5000/customers" # should be in config file

print("Hello orders manager:")
while True:
    print('1. Show all customers')
    print('2. Show customer by id')
    print('3. Delete customer by id')
    print('4. Add new customer')
    print('5. Update customer')
    print('0. Exit')
    choice = input("What's your choice?")
    if choice == "0":
        break;
    if choice == "1":
        resp = requests.get(url)
        if resp.status_code != 200:
            print('Unexpected error')
            continue
        customers = json.loads(resp.content)
        for c in customers:
            print(c)
    if choice == "2":
        cust_id = input("What's the cusgtomer id to get?")
        resp = requests.get(f'{url}/{cust_id}')
        if resp.status_code != 200:
            print('Unexpected error')
            continue
        customer1 = json.loads(resp.content)
        if (customer1 == []):
            print('-- Not found --')
        else:
            print(customer1)
    if choice == "3":
        cust_id = input("What's the customer id to delete?")
        resp = requests.delete(f'{url}/{cust_id}')
        if resp.status_code != 200:
            print('Unexpected error')
            continue
        customer1 = json.loads(resp.content)
        if (customer1 == []):
            print('-- Not found --')
        else:
            print(customer1)
    if choice == "4":
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        names_par = ['name', 'age', 'address', 'salary']
        new_cust = {}
        for par in names_par:
            value = input(f'Please insert {par}: ')
            new_cust[par] = value
        #new_cust = {'name': 'yair3', 'age': '28', 'address': 'petah tikva3', 'salary': '60000'}
        resp = requests.post(url, data=json.dumps(new_cust), headers=headers)
    if choice == "5":
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        cust_id = input("What's the cusgtomer id to update?")
        names_par = ['name', 'age', 'address', 'salary']
        updated_cust = {}
        for par in names_par:
            value = input(f'Please insert {par}: ')
            updated_cust[par] = value
        #new_cust = {'name': 'yair3', 'age': '28', 'address': 'petah tikva3', 'salary': '60000'}
        resp = requests.put(f'{url}/{cust_id}', data=json.dumps(updated_cust), headers=headers)